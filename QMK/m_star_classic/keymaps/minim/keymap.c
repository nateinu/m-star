/*

TODO: It seems to be possible to get the NumLock LED and layer states out of
sync when quickly tapping the ScrLk key. Longer presses seem to be fine.

*/

#include QMK_KEYBOARD_H

enum layers {
    BASE,
    NUMPAD
};

// clang-format off
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [BASE] = LAYOUT_minim(
    KC_ESC,   KC_F1, KC_F2, KC_F3, KC_F4,   KC_F5, KC_F6, KC_F7, KC_F8,   KC_F9, KC_F10, KC_F11, KC_F12,         KC_PSCR, KC_SLCK, KC_PAUS,
    KC_GRV, KC_1,  KC_2,  KC_3,  KC_4,  KC_5,  KC_6,  KC_7,  KC_8,  KC_9,  KC_0,  KC_MINS, KC_EQL,  KC_BSPC,     KC_INS,  KC_HOME, KC_PGUP,
    KC_TAB,   KC_Q,  KC_W,  KC_E,  KC_R,  KC_T,  KC_Y,  KC_U,  KC_I,  KC_O,  KC_P,  KC_LBRC, KC_RBRC, KC_BSLS,   KC_DEL,  KC_END,  KC_PGDN,
    KC_CAPS,    KC_A,  KC_S,  KC_D,  KC_F,  KC_G,  KC_H,  KC_J,  KC_K,  KC_L,  KC_SCLN, KC_QUOT,    KC_ENT,
    KC_LSPO,      KC_Z,  KC_X,  KC_C,  KC_V,  KC_B,  KC_N,  KC_M,  KC_COMM, KC_DOT,  KC_SLSH,    KC_RSPC,                 KC_UP,
    KC_LCTL, KC_LGUI, KC_LALT,                KC_SPC,           KC_RALT, KC_RGUI, KC_APPLICATION, KC_RCTL,       KC_LEFT, KC_DOWN, KC_RGHT
  ),
  [NUMPAD] = LAYOUT_minim(
    _______,        _______, _______, _______, _______,        _______, _______, _______, _______,        _______, _______, _______, _______,  _______, _______, _______,
    _______, _______,  _______,  _______,  _______,  _______,  _______,  KC_P7,   KC_P8,   KC_P9,  _______,  KC_PMNS, KC_PPLS,  _______,       _______, _______, _______,
    _______,   _______,  _______,  _______,  _______,  _______,  _______,  KC_P4,    KC_P5,    KC_P6,    _______,  _______, _______, _______,  _______, _______,  _______,
    _______,    _______,  _______,  _______,  _______,   _______,  _______,  KC_P1,   KC_P2,    KC_P3,    KC_PAST, _______,    _______,
    _______,       _______,  _______,  _______,  _______,  _______,  _______,  KC_P0,    _______, KC_PDOT,  KC_PSLS,        _______,                    _______,
    _______, _______, _______,                             _______,                                       _______, _______, _______, _______,  _______, _______, _______
  )
};
// clang-format on

void keyboard_post_init_user(void) {
    /* Sync the layer state to the numpad state of the host system */
    led_t led_state = host_keyboard_led_state();
    if (led_state.num_lock) {
        layer_on(NUMPAD);
    } else {
        layer_off(NUMPAD);
    }
}

/* For the front-of-key legends on the numpad keys */
#define REG_OR_UNREG(key) { \
    if (record->event.pressed) { \
        del_mods(MOD_MASK_SHIFT); \
        register_code(key); \
        set_mods(mod_state); \
        return false; \
    } else { \
        del_mods(MOD_MASK_SHIFT); \
        unregister_code(key); \
        set_mods(mod_state); \
        return false; \
    } \
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    uint8_t mod_state = get_mods();
    /* All the interesting stuff requires the shift key being pressed */
    if ((mod_state & MOD_MASK_SHIFT) == 0) {
        return true;
    }
    switch(keycode) {
    case KC_SLCK:
        /* Shift + Scroll Lock = Num Lock */
        /* Enable / Disable the numpad layer */
        if (record->event.pressed) {
            if (layer_state_is(NUMPAD)) {
                layer_off(NUMPAD);
            } else {
                layer_on(NUMPAD);
            }
            register_code(KC_NUM_LOCK);
        } else {
            unregister_code(KC_NUM_LOCK);
        }
        break;
    case KC_P1:
        REG_OR_UNREG(KC_END)
    case KC_P2:
        REG_OR_UNREG(KC_DOWN)
    case KC_P3:
        REG_OR_UNREG(KC_PGDN)
    case KC_P4:
        REG_OR_UNREG(KC_LEFT)
    case KC_P5:
        REG_OR_UNREG(KC_P5)
    case KC_P6:
        REG_OR_UNREG(KC_RGHT)
    case KC_P7:
        REG_OR_UNREG(KC_HOME)
    case KC_P8:
        REG_OR_UNREG(KC_UP)
    case KC_P9:
        REG_OR_UNREG(KC_PGUP)
    case KC_P0:
        REG_OR_UNREG(KC_INS)
    case KC_PDOT:
        REG_OR_UNREG(KC_DEL)
    default:
        break;
    }
    return true;
    // Use hid_listen to debug:
    // uprintf("KL: kc: 0x%04X, col: %u, row: %u, pressed: %b, time: %u, interrupt: %b, count: %u\n", keycode, record->event.key.col, record->event.key.row, record->event.pressed, record->event.time, record->tap.interrupted, record->tap.count);
}

void matrix_init_user(void) {
    // This is very important to let the user know that the QMK keymap is
    // loaded and "home" -- could help troubleshoot matrix connection issues.
    // Each LED comes on 250ms after the other.
    writePin(B9, false);
    wait_ms(250);
    writePin(B8, false);
    wait_ms(250);
    writePin(B7, false);
    wait_ms(250);
}
