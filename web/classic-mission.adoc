= Mission statement

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

* Develop a drop-in replacement controller PCB for vintage Model Ms
  that natively ships USB and holds power consumption to levels that
  don't cause problems with modern USB hubs.

* Support the maximum range of Ms possible, giving priority to vintage
  Ms that ship PC/AT and PS/2.

* Emphasize good production engineering so the design can be
  manufactured in volume at low cost.

* Run QMK so remapping and macros are available with a well-developed
  ecosystem of documentation and tools.

This project will not be considered complete until the resulting
design is not merely proven but actually available for sale to people
restoring vintage Model Ms.

//end
