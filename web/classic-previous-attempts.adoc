= Previous attempts

There have been quite a number of previous attempts at a replacement M controller.
We've collected links here so that project contributors can study and learn from them.
They're listed in the order we learned of them, except for ours in last place.

When these projects didn't give themselves unique distinguishing names
we've tagged them with the name of their creator.

How to read the "License" column:

1. All controllers with in-tree QMK support hasve software licensed
under GPL2, and the "License" column applies only to the CAD files.

2. If it indicates an open-source license, the CAD files have been
published under that license.  If the software is not in the QMK
tree, so has the software.

3. "-" means no design files have been published.

include::attempts-table.adoc[]

== Notes

=== The Ashpil controller

As of 2021-07-12 DigiKey thinks it can ship the MCU 7/27/2021,
so the part is available.

The repository for this project includes a form to order the parts from
DigiKey.

=== The Yugo controller

Optional daughterboards support output as Micro-B or C

As of 2021-07-23, this project is on hold until the STM32F303 becomes available again.

=== The Schwingen controller

Michael has joined the M-Star Classic project and is doing our
link:roadmap.html[unitary design].

=== The Model H

As of 2021-07-23, this project is on hold until the STM32 becomes available again.

The project web page expresses the intent to be open source, but as
of 2021-08 https://github.com/jhawthorn/modelh/issues/3[there is no
license file].

Maintainer doesn't answer email.

=== The Rump controller

There is something called the OBDEV licemse applying to the USB stack
that purports tro extend GPL3 to hardware.

Maintainer doesn't answer email.

=== The Dulcimer controller

Dates from 2008. Seems inactive.

=== The Phosphorglow controller

Author calls it "The Universal M Controller".

Preannounced in 2015, no public activity since.

Was expected to run Soarer's Controller/TMK/homebrew
firmware but the preannouncement implies it was never ported.

Has a prototyping area.

Design files never published.

=== BT-USB-Hybrid controller

Design files were never published, the parts were hand-made
and sold as kits.  It's unclear what the firmware was, probably
custom. The announcement phootos aren't clear enough for the MCU
to be identified.

Broad variant support - M101, M122, press-fit controller via daughterboard.

Included an LED daughterboard.

Claims Bluetooth support, but it's unclear how that's powered. 

=== Sunxui aftermarket controller

Proprietary. Carrier-board design, the engine looks like a Blue Pill or Black Pill
controller clone.  Unclear what the firmware is. 

Features a solenoid controller in case your M isn't loud enough.

===  JieKeBo controllers

https://deskthority.net/viewtopic.php?f=50&t=25592

Announced 2021.  Two prototypes, one for press-fit membranes
one for older M101s/M122s.  The second is a carrier-bord design
using a Teensy.

No design files.  Firmware unspecified.

== Breadboard mods

There have beehn innumerable controller-replacement mods using hobby breadboards.
https://github.com/antonizoon/antonizoon.github.io/wiki/IBM-Model-M-USB-Controller[This one]
and https://deskthority.net/viewtopic.php?f=7&t=8149[this one] are representative.
