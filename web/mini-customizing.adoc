= Customizing your M-Star-Mini-equipped keyboard  =

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

Out of the box, a keyboard equipped with a M-Star Mini controller will
behave exactly like a stock Unicomp Mini-M, except without
intermittently dropping the Q key and other bugs such as not
activating when your computer comes out of sleep mode.

You need read no further unless you want to use the capabilities of
the QMK firmmare running on your M-Star Mini to change your keymap or
set up macros.

== Setting up QMK

You'll need to start by reading https://docs.qmk.fm/#/newbs[The QMK
Tutorial]. Unfortunately, the QMK online GUI does not yet support
the M-Star Mini; you will have to build your firmware by hand.

// FIXME: Check paths and "m-star-mini" before shipping
To finish your preparation, copy or symlink the contents of the
directory QMK/m-star-mini in the M-Star repository into
keyboards/mini in the QMK repository.  A typical command for a
default setup, if MYPATH is set to where you keep yor M-Star Mini code,
would be:

----------------------------------------------------------
$ ln -sf $MYPATH/QMK/m_star-mini ~/qmk_firmware/keyboards/m_star_mini
----------------------------------------------------------

This lets you use QMK to build and flash your keyboard firmware.

== Reflashing your keyboard firmware

Warning: if you have two M-Star-Mini-equipped keyboards plugged in when
the flash command runs, QMK can get confused about which one to ship
to! So don't do that.

Our keyboard type is "m-star-mini".  To find out what keymaps are defined, see
https://gitlab.com/esr/m-star/-/tree/main/QMK/m-star-mini[this
list].

This example builds and flashes the default keymap, but you can give
the name of any variant you like as a last argument.

-----------------------------------------
$ qmk flash -kb m_star_mini -km default
-----------------------------------------

You should see output similar to:

-------------------------------------------------------
Compiling keymap with gmake -j 1 m-star-mini:default:flash

QMK Firmware 0.13.28
Making m-star-mini with keymap default and target flash

arm-none-eabi-gcc (15:9-2019-q4-0ubuntu2) 9.2.1 20191025 (release) [ARM/arm-9-branch revision 277599]
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Size before:
   text    data     bss     dec     hex filename
      0   35762       0   35762    8bb2 .build/m-star-mini_default.hex

Compiling: quantum/command.c                                                                        [OK]
Compiling: quantum/process_keycode/process_terminal.c                                               [OK]
Linking: .build/m-star-mini_wendell.elf                                                             [OK]
Creating binary load file for flashing: .build/m-star-mini_wendell.bin                              [OK]
Creating load file for flashing: .build/m-star-mini_default.hex                                     [OK]

Size after:
   text    data     bss     dec     hex filename
      0   35762       0   35762    8bb2 .build/m-star-mini_default.hex

Copying m-star-mini_default.bin to qmk_firmware folder                                              [OK]
-------------------------------------------------------

When you see this message...

-------------------------------------------------------
Bootloader not found. Trying again every 0.5s (Ctrl+C to cancel)...........
-------------------------------------------------------

...simply unplug and replug your keyboard. On bootup the keyboard
waits for 250msec to download a new firmware image before resuming the boot
operation (for now, as a failsafe).

//end
