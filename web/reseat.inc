Put the front edge of the plate assembly under the retaining fins at
the front of the pan.  Let the rear end drop slowly towards its
support posts at the rear of the case.  Guide your M-Star PCB so it ends
up sitting loose inside the pan, under the backplate.  It's OK if the
backplate is partly sitting on the PCB as well as a post or two
when it comes to rest, the PCB does not crush easily.

Now lift the back edge of the plate assembly an inch or two with your
non-dominant hand, just enough that you can reach under it with ypur
dominant hand but not enough to put any strain on the ribbon cables.

With the rear edge of the plate assembly elevated just enough to give
the M-Star Mini room to be moved around, position the M-Star Mini so you can
see the USB jack through the aperture and the holes on either side of
its USB jack line up with the mounting pegs.  If you have the plate
assembly positioned correctly, this should be a small adjustmemt.

Now actually press down gently so the mounting pegs fill those holes.
But watch the amount of slack in the ribbon cables carefully;  you
do *not* want to put enough strain on them that they pull out of
the sockets on the PCB, because each insertion is a seriously
lifetime-shortening event.

So if you feel pull from the ribbon cables as you're moving or seating the
board, *stop*. Change your hold on the plate assembly so it's resting
at a lower angle (which gives the ribbon cables more slack) then try
again.  Repeat until you get the board sitting on the pan surface
and properly registered on the mounting pegs.  
