= M-Star Project Roadmap =

Here are the project's concrete goals and plans:

== Gather and verify interface specifications

We've made a decision to stick with the full-sized USB B jack for
output.  Mini- and micro-B are passing out of use in favor of C, and C
has a problem with divergent subtypes.

We need to know everything about the link:interfaces.html[physical and
logical interfaces] of the M over the entire range of variants within
coverage.  Eric is responsible for collecting and curating this
information.

This part is actually almost done. We're chasing down some last bits
of information about odd variants.  We also need to fully document
LED voltages and pinouts.

Brandon's main job before testing is to reality-check what we think we
know.

== Carrier-board designs for the Classic and Mini 

The silicon shortage has an impact. Bare MCUs in the class we need
(ATmega32 or STM32) have huge lead times due to the silicon shortage.
It is still possible to buy dev boards

Prototypes exist in the repository.

This will be our stopgap at least until the silicon shortage eases.

The Classic is a single PCB layout that, depending on mounted components,
will work with:
    Terminal Model M
    Terminal Model M/122 
    Model M
    Model M SSK
    Model M 122
    Model M AT/122

The Mini M is a single PCB layout that works with ONLY the 2021 "Mini M" from Unicomp. 

== Testing

The Classic and Mini installation instructions need to be tested on someone who is not
one of the dev group.

== Unitary design for Classic

Michael already has a working replacement controller. When we can get
the parts, we'll revisit what can be done in a unitary design that lowers
the unit cost and preserves features relative to the carrier-board design.

Planned modifications of his design:

* The reset button to invoke the bootloader will be eliminated in
  favor of a magic keystroke.

== ZIF connectors

FFC insertion is too risky for a significant fraction of our userbase.
We need a source for ZIF connectors with an 0.1" pitch at 8, 12, 16
and 20-pin sizes, which is awkward as this pitch is obsolete.

We have a number of parts inbound from TE, Amphenol and others to test. 

Here's
https://www.te.com/global-en/plp/zif-line/X27rJ.html?q=&d=545000%20586273&type=products&samples=N&inStoreWithoutPL=false&instock=N[Triomate's listings]

== Press-fit adapter

Wendell has a prototype in the repo of an adapter that will mate
press-fit membranes to a Classic.

This needs to be tested and documented.

== Productization

This website needs the following documentation:

Installation instructions for finished Classic and Mini::
   Classic instructions are essentially complete, need testing and some
   images filled in. Mini instructions arec started but need work.

How to flash QMK onto the board::
   We have a Classic customization page finished.  The Mini
   instructions are started but need work.

How to build the board(s) from a kit::
   Not yet started.

Hacker's guide::
   How to set up to assist in development.  Started, needs more work.

Eric is taking responsibility for these.

Do redirect from m-star.io to the GitLab pages.

We'll want to make sure the Classic and Mini are supported by the QMK web
configurator, and update the Customizing page to reflect this.  This
means pushing our QMK tree to the QMK firmware repo.

Trade-offs about selling finished boards vs. kits need to be
considered, but not until we have a working design.

Michael notes: "If you intend to ship these to Germany, then a
partially assembled kit (SMD assembled, but THT connectors have to be
soldered by the user) might be useful: AFAIR, that way it is not
considered a 'finished product' and can get around the
https://www.ecosurety.com/news/what-you-need-to-know-about-weee-compliance-in-germany-part-one/[WEEE
regulation], but IANAL."

Shop link on website.

== Marketing

Final choice of logo.

Do a Kickstarter?

Publicity: deskthority, geekhack, r/modelm, level1techs, Slashdot

== Wishlist items

These are not yet on the roadmap, but might be promoted to it.

* Breakaway board section to replace LED daughterboard.

* Documented support for RGB LEDs?

* Add a prototyping pad to both PCB designs for things like authentication hardware?

* Ship 5.5mm nut drivers with the board?  We can get them very cheap
  in bulk.  Note, make sure they're thin-walled eniugh to fit in the
  case wells.

* Solenoid controller?  Might not fit in our power budget.  Maybe
  just leave unpopulated features on the PCB.

// end
